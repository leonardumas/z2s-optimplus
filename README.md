**z2s-optimplus** version 0.6.20210531.1

imported from https://gitlab.utc.fr/crozatst/z2s

# AUTHORS

Stéphane Crozat http://stph.crzt.fr

Léonard Dumas

# LICENCE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


# INSTALL


1. Retrieve a ZIP file with the converter
    * using a Git client 
    * or using download icon in web site (upper right, next to "Find file" button)
1. Uncompress the resulting ZIP file anywere on your computer


# MANUAL

1. Export Zotero Library choosing "Format TEI" (any options)
1. Copy resulting XML file into "input" folder
1. Clic `run-optimplus.bat` (Windows) or execute `sh ./run-optimplus.sh` (Linux, Mac)
1. Retrieve result in folder output/zotero-optimplus : "Bibliographie" items for OptimPlus 