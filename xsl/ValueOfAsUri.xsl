<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">

    <!--****************************************************************
   Last update : 2015/05/13
        Add translate / to -
        Add translate – to -
   *****************************************************************-->
    
    <xsl:template name="ValueOfAsURI">
        <xsl:param name="pInput"></xsl:param>             
        <xsl:value-of select="
            translate(
            normalize-space($pInput),
            ' AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZzÀàÁáÂâÃãÄäÅåÆæÇçÈèÉéÊêËëÌìÍíÎîÏïÐðÑñÒòÓóÔôÕõÖöØøÙùÚúÛûÜüÝýÞþ–/',
            '_aabbccddeeffgghhiijjkkllmmnnooppqqrrssttuuvvwwxxyyzzaaaaaaaaaaaaaacceeeeeeeeiiiiiiiioonnoooooooooooouuuuuuuuyyzz--'                
            )"/>
    </xsl:template>
</xsl:stylesheet>