<?xml version="1.0" encoding="UTF-8"?>
<!--
<a href="http://stph.crzt.fr">Stéphane Crozat</a> 
<a href="http://www.gnu.org/licenses/gpl-3.0.txt">License GPL3.0</a>
Jun 14, 2015
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"    
    version="2.0"
    xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
    xmlns:op="utc.fr:ics/opale3" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    
    <xsl:param name="pOutputUrl">file:///tmp</xsl:param>
    
    <xsl:template match="bib">
        <xsl:apply-templates select="ref"/>
        <message>Output set to <xsl:value-of select="$pOutputUrl"/></message>
    </xsl:template>
    
    <xsl:template match="ref">
        <xsl:result-document href="{$pOutputUrl}/{ref-id}.ref" encoding="UTF-8" method="xml" >
            <sc:item>
                <op:bib>
                   <op:bibM>
                       <sp:id><xsl:value-of select="ref-name"/></sp:id>
                       <sp:type>bib</sp:type>
                       <sp:desc>
                           <op:bibTxt>
                               <sc:para xml:space="preserve"><xsl:apply-templates select="*"/></sc:para>
                           </op:bibTxt>
                       </sp:desc>
                   </op:bibM>
                </op:bib>
            </sc:item>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="structured-data | ref-id | ref-name"/>
      
    <xsl:template match="authors">
        <xsl:if test="text()!=''">
            <sc:textLeaf role="auth">
                <xsl:value-of select="."/>            
            </sc:textLeaf>
            <xsl:text>. </xsl:text>
        </xsl:if>
    </xsl:template>  
      
    <xsl:template match="title">
        <xsl:if test="text()!=''">
            <sc:textLeaf role="title">
                <xsl:value-of select="."/>            
            </sc:textLeaf>
            <xsl:text>. </xsl:text>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="edition">
        <xsl:if test="text()!=''">
            <sc:textLeaf role="ed">
                <xsl:value-of select="."/>            
            </sc:textLeaf>
            <xsl:text>. </xsl:text>
        </xsl:if>
    </xsl:template>  
    
    <xsl:template match="year">
        <xsl:if test="text()!=''">
            <sc:textLeaf role="date">
                <xsl:value-of select="."/>            
            </sc:textLeaf>
            <xsl:text>. </xsl:text>
        </xsl:if>
    </xsl:template>  
    
    <xsl:template match="url">
        <xsl:if test="text()!=''">
            <sc:uLink role="url" url="{.}">
                <xsl:value-of select="."/>            
            </sc:uLink>
            <xsl:text>. </xsl:text>
        </xsl:if>
    </xsl:template>
        
    <xsl:template match="*">
        <xsl:if test="text()!=''">
           <xsl:value-of select="."/>
           <xsl:text>. </xsl:text>
        </xsl:if>
    </xsl:template>
    
</xsl:stylesheet>