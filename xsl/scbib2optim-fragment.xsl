<?xml version="1.0" encoding="UTF-8"?>
<!--
<a href="http://stph.crzt.fr">Stéphane Crozat</a> 
<a href="http://www.gnu.org/licenses/gpl-3.0.txt">License GPL3.0</a>
Aug 31, 2015
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"    
    version="2.0"
    xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
    xmlns:of="scpf.org:office" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    
	<xsl:param name="pOutputUrl">file:///tmp</xsl:param>
    
    <xsl:template name="section">
    	<xsl:param name="title"/>    
    	<xsl:param name="type"/>	
   		<sp:sec>
			<of:section>					
				<of:sectionM>
					<sp:title><xsl:value-of select="$title"/></sp:title>
				</of:sectionM>
				<sp:content>
					<of:fragment>
						<xsl:apply-templates select="ref[@type=$type]" mode="section">
							<xsl:sort select="year" order="descending"/>
						</xsl:apply-templates>
					</of:fragment>
				</sp:content>
			</of:section>
		</sp:sec>
    </xsl:template>
    
    <xsl:template match="bib">
    	<!--  Section -->
		<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
			<of:section xmlns:of="scpf.org:office" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
				<of:sectionM>
					<sp:title>Bibliographie</sp:title>
				</of:sectionM>
				<!--  book -->
				<xsl:call-template name="section">
					<xsl:with-param name="title">Livres</xsl:with-param>
					<xsl:with-param name="type">book</xsl:with-param>									
				</xsl:call-template>
				<!--  bookSection -->
				<xsl:call-template name="section">
					<xsl:with-param name="title">Chapitres de livre</xsl:with-param>
					<xsl:with-param name="type">bookSection</xsl:with-param>									
				</xsl:call-template>				
				<!--  journalArticle -->
				<xsl:call-template name="section">
					<xsl:with-param name="title">Articles de revues</xsl:with-param>
					<xsl:with-param name="type">journalArticle</xsl:with-param>									
				</xsl:call-template>											
				<!--  conferencePaper -->
				<xsl:call-template name="section">
					<xsl:with-param name="title">Articles de conférence</xsl:with-param>
					<xsl:with-param name="type">conferencePaper</xsl:with-param>									
				</xsl:call-template>					
				<!--  presentation -->
				<xsl:call-template name="section">
					<xsl:with-param name="title">Communications orales</xsl:with-param>
					<xsl:with-param name="type">presentation</xsl:with-param>									
				</xsl:call-template>		
				<!--  presentation -->
				<xsl:call-template name="section">
					<xsl:with-param name="title">Mémoires</xsl:with-param>
					<xsl:with-param name="type">thesis</xsl:with-param>									
				</xsl:call-template>		
				<!--  Autres -->
				<sp:sec>
					<of:section>					
						<of:sectionM>
							<sp:title>Autres publications et communications</sp:title>
						</of:sectionM>
						<sp:content>
							<of:fragment>
								<xsl:apply-templates select="ref[not(@type)] 
															| ref[@type!='book' 
															and @type!='bookSection' 
															and @type!='journalArticle' 
															and @type!='conferencePaper'
															and @type!='presentation'
															]"
															mode="section"></xsl:apply-templates>
							</of:fragment>
						</sp:content>
					</of:section>
				</sp:sec>
			</of:section>
		</sc:item>
    	<!--  Fragment -->
        <xsl:apply-templates select="ref"/>
                
    </xsl:template>
    
    <xsl:template match="ref" mode="section">
    	<sp:frag sc:refUri="co/{ref-id}.fragment"/>
    </xsl:template>
    
    <xsl:template match="ref">
        <xsl:result-document href="{$pOutputUrl}/{ref-id}.fragment" encoding="UTF-8" method="xml" >
           	<sc:item xmlns:sc="http://www.utc.fr/ics/scenari/v3/core">
				<of:fragment xmlns:of="scpf.org:office" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
					<sp:info>
						<of:block>
							<of:blockM/>
							<sp:co>
								<of:flow>
									<sp:txt>
										<of:txt>
				                            <sc:para xml:space="preserve"><xsl:apply-templates select="*"/></sc:para>
			                      		</of:txt>
									</sp:txt>
								</of:flow>
							</sp:co>
						</of:block>
					</sp:info>
				</of:fragment>
            </sc:item>  
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="structured-data | ref-id | ref-name"/>
      
    <xsl:template match="title">
        <xsl:if test="text()!=''">
            <sc:inlineStyle role="emphasis">
                <xsl:value-of select="."/>            
            </sc:inlineStyle>
            <xsl:text>. </xsl:text>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="url">
        <xsl:if test="text()!=''">
            <sc:phrase role="url">
				<of:urlM xmlns:of="scpf.org:office">
					<sp:url xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive"><xsl:value-of select="."/></sp:url>
				</of:urlM>
				<xsl:value-of select="."/>
			</sc:phrase>
            <xsl:text>. </xsl:text>  
        </xsl:if>
    </xsl:template>
        
    <xsl:template match="*">
        <xsl:if test="text()!=''">
           <xsl:value-of select="."/>
           <xsl:text>. </xsl:text>
        </xsl:if>
    </xsl:template>
    
</xsl:stylesheet>