<?xml version="1.0" encoding="UTF-8"?>
<!--
<a href="http://stph.crzt.fr">Stéphane Crozat</a> 
<a href="http://www.gnu.org/licenses/gpl-3.0.txt">License GPL3.0</a>
Sep 03, 2015
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"    
    version="2.0"
    xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
    xmlns:of="scpf.org:office" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    
    <xsl:param name="pOutputUrl">file:///tmp</xsl:param>
    
    <xsl:template match="bib">
        <xsl:apply-templates select="ref"/>
        <message>Output set to <xsl:value-of select="$pOutputUrl"/></message>
    </xsl:template>
    
    <xsl:template match="ref">
        <xsl:result-document href="{$pOutputUrl}/{ref-id}.refs" encoding="UTF-8" method="xml" >
            <sc:item>
                <of:def>
                   <of:defM>
                       <sp:term><xsl:value-of select="ref-name"/></sp:term>
                   </of:defM>
                    <sp:def>
                        <of:sTxt>
                            <sc:para xml:space="preserve"><xsl:apply-templates select="*"/></sc:para>
                        </of:sTxt>
                    </sp:def>
                </of:def>
            </sc:item>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="structured-data | ref-id | ref-name"/>
      
    <xsl:template match="title">
        <xsl:if test="text()!=''">
            <sc:phrase role="special">
                <xsl:value-of select="."/>            
            </sc:phrase>
            <xsl:text>. </xsl:text>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="url">
        <xsl:if test="text()!=''">
        	<sc:phrase role="url">
        		<of:urlM>
        			<sp:url><xsl:value-of select="."/></sp:url>
        			<sp:title><xsl:value-of select="../title"/></sp:title>
       			</of:urlM>
       			<xsl:value-of select="."/>
    		</sc:phrase>
    		<xsl:text>.</xsl:text>       
        </xsl:if>
    </xsl:template>
        
    <xsl:template match="*">
        <xsl:if test="text()!=''">
           <xsl:value-of select="."/>
           <xsl:text>. </xsl:text>
        </xsl:if>
    </xsl:template>
    
</xsl:stylesheet>