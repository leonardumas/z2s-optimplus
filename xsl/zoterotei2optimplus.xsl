<?xml version="1.0" encoding="UTF-8"?>
<!--
<a href="http://stph.crzt.fr">Stéphane Crozat</a> 
<a href="http://www.gnu.org/licenses/gpl-3.0.txt">License GPL3.0</a>
Jun 14, 2015
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"    
    version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
		xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
		xmlns:of="scpf.org:office" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    
    <xsl:output 
        indent="yes" 
        encoding="UTF-8"
        method="xml"/>

		<xsl:include href="zoterotei2scbib.xsl"/>
		<xsl:param name="pOutputUrl">file:///tmp</xsl:param>

	<!--****************************************************************
	Main
	*****************************************************************-->
	<xsl:template match="/">
		<xsl:processing-instruction name="xml-stylesheet">type"text/xsl" href="scbib2optimplus.xsl"</xsl:processing-instruction>
		<xsl:apply-templates select=".//biblStruct"></xsl:apply-templates>
		<message>Output set to <xsl:value-of select="$pOutputUrl"/></message>
	</xsl:template>

	<xsl:template match="biblStruct">
		<xsl:variable name="refId">
			<xsl:call-template name="GenerateRefId"/>
		</xsl:variable>
		<xsl:result-document href="{$pOutputUrl}/{$refId}.bib" encoding="UTF-8" method="xml" >
			<sc:item>
				<xsl:apply-templates select="." mode="inner"/>
			</sc:item>
		</xsl:result-document>
	</xsl:template>

	<xsl:template name="titleAndAuthors">
		<sp:title>
			<xsl:apply-templates select=".//title[parent::analytic or not(../preceding-sibling::*/title)][1]" mode="MainTitle"/>
		</sp:title>
        <!-- Authors -->
		<xsl:for-each select=".//author">
			<sp:author>
				<xsl:call-template name="GetName">
					<xsl:with-param name="pMode">surname-forename</xsl:with-param>
				</xsl:call-template>
			</sp:author>
		</xsl:for-each>
        <!-- Editor are added if no author found --> 
        <xsl:if test="not(.//author)">
			<xsl:for-each select=".//editor">
				<sp:author>
					<xsl:call-template name="GetName">
						<xsl:with-param name="pMode">surname-forename</xsl:with-param>
					</xsl:call-template>
				</sp:author>
			</xsl:for-each>
        </xsl:if>
 		<!-- Contributors are added if no author nor editor found -->
        <xsl:if test="not(.//author | .//editor)">
			<xsl:for-each select=".//persName">
				<sp:author>
					<xsl:call-template name="GetName">
						<xsl:with-param name="pMode">surname-forename</xsl:with-param>
					</xsl:call-template>
				</sp:author>
			</xsl:for-each>
        </xsl:if>
	</xsl:template>

	<xsl:template name="pages">
		<xsl:for-each select=".//biblScope[@unit='page']">
			<sp:pages><xsl:value-of select="."/></sp:pages>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="volume">
		<xsl:for-each select=".//biblScope[@unit='volume']">
			<sp:volume>
				<xsl:value-of select="."/>
				<xsl:for-each select="../biblScope[@unit='issue']">(<xsl:value-of select="."/>)</xsl:for-each>
			</sp:volume>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="publisher">
		<xsl:choose>		
			<xsl:when test=".//publisher">
				<sp:publisher><xsl:value-of select=".//publisher"/></sp:publisher>
			</xsl:when>
			<xsl:otherwise>
				<xsl:for-each select=".//edition">
					<sp:publisher><xsl:value-of select="."/></sp:publisher>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="collection">
		<xsl:if test=".//publisher and .//edition">
			<sp:collection><xsl:value-of select=".//edition"/></sp:collection>
		</xsl:if>
	</xsl:template>

	<xsl:template name="series">
		<xsl:for-each select=".//series/title">
			<sp:series><xsl:apply-templates/></sp:series>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="thesisType">
		<xsl:for-each select=".//note[@type='thesisType']">
			<sp:type><xsl:value-of select="normalize-space(.)"/></sp:type>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="place">
		<xsl:for-each select=".//pubPlace">
			<sp:place><xsl:apply-templates/></sp:place>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="idno">
		<xsl:param name="pType"/>
		<xsl:for-each select=".//idno[@type=$pType]">
			<xsl:element name="sp:{lower-case($pType)}"><xsl:value-of select="normalize-space(.)"/></xsl:element>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="url">
		<xsl:for-each select=".//note[@type='url']">
			<sp:url><xsl:value-of select="normalize-space(.)"/></sp:url>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="extra">
		<xsl:variable name="vEditors" select=".//author and .//editor"/>
		<xsl:variable name="vContributors" select="(.//author or .//editor) and .//persName"/>
		<xsl:if test="$vEditors or $vContributors">
			<sp:extra>
				<of:txt>
			        <xsl:if test=".//author and .//editor">
			        	<sc:para>
							<xsl:for-each select=".//editor">
								<xsl:call-template name="GetName">
									<xsl:with-param name="pMode">surname-forename</xsl:with-param>
								</xsl:call-template>
								<xsl:if test="following-sibling::editor">, </xsl:if>
							</xsl:for-each>
			        	</sc:para>
			        </xsl:if>
			        <xsl:if test="(.//author or .//editor) and .//persName">
			        	<sc:para>
							<xsl:for-each select=".//persName">
								<xsl:call-template name="GetName">
									<xsl:with-param name="pMode">surname-forename</xsl:with-param>
								</xsl:call-template>
								<xsl:if test="../following-sibling::*/persName">, </xsl:if>
							</xsl:for-each>
			        	</sc:para>
			        </xsl:if>
			    </of:txt>
			</sp:extra>
		</xsl:if>
	</xsl:template>

	<xsl:template match="biblStruct[@type='journalArticle' or @type='newspaperArticle' or @type='magazineArticle']" mode="inner">
		<of:article>
			<of:articleM>
				<xsl:call-template name="titleAndAuthors"/>
				<sp:journal>
					<xsl:apply-templates select=".//title[parent::monogr and ../preceding-sibling::*/title]" mode="MainTitle"/>
				</sp:journal>
				<sp:date>
					<xsl:call-template name="GenerateYear"/>
				</sp:date>
				<xsl:call-template name="volume"/>
				<xsl:call-template name="pages"/>
				<xsl:call-template name="url"/>
				<xsl:call-template name="idno"><xsl:with-param name="pType">DOI</xsl:with-param></xsl:call-template>
				<xsl:call-template name="idno"><xsl:with-param name="pType">ISSN</xsl:with-param></xsl:call-template>
				<xsl:call-template name="extra"/>
			</of:articleM>
		</of:article>
	</xsl:template>

	<xsl:template match="biblStruct[@type='book']" mode="inner">
		<of:book>
			<of:bookM>
				<xsl:call-template name="titleAndAuthors"/>
				<xsl:call-template name="publisher"/>
				<sp:date>
					<xsl:call-template name="GenerateYear"/>
				</sp:date>
				<xsl:call-template name="volume"/>
				<xsl:call-template name="series"/>
				<xsl:call-template name="place"/>
				<xsl:call-template name="collection"/>
				<xsl:call-template name="idno"><xsl:with-param name="pType">ISBN</xsl:with-param></xsl:call-template>
				<xsl:call-template name="url"/>
				<xsl:call-template name="extra"/>
			</of:bookM>
		</of:book>
	</xsl:template>
	
	<xsl:template match="biblStruct[@type='bookSection']" mode="inner">
		<of:bookSection>
			<of:bookSectionM>
				<xsl:call-template name="titleAndAuthors"/>
				<sp:booktitle>
					<xsl:apply-templates select=".//title[parent::monogr and ../preceding-sibling::*/title]" mode="MainTitle"/>
				</sp:booktitle>
				<!-- bookauthor -->
				<xsl:call-template name="pages"/>
				<xsl:call-template name="publisher"/>
				<sp:date>
					<xsl:call-template name="GenerateYear"/>
				</sp:date>
				<xsl:call-template name="volume"/>
				<xsl:call-template name="series"/>
				<xsl:call-template name="place"/>
				<!-- edition -->
				<xsl:call-template name="idno"><xsl:with-param name="pType">ISBN</xsl:with-param></xsl:call-template>
				<xsl:call-template name="url"/>
				<xsl:call-template name="extra"/>
			</of:bookSectionM>
		</of:bookSection>
	</xsl:template>

	<xsl:template match="biblStruct[@type='conferencePaper']" mode="inner">
		<of:conferencePaper>
			<of:conferencePaperM>
				<xsl:call-template name="titleAndAuthors"/>
				<sp:proceedingstitle>
					<xsl:apply-templates select=".//title[parent::monogr and ../preceding-sibling::*/title][1]" mode="MainTitle"/>
				</sp:proceedingstitle>
				<!-- proceedingseditor -->
				<xsl:call-template name="publisher"/>
				<sp:date>
					<xsl:call-template name="GenerateYear"/>
				</sp:date>
				<xsl:call-template name="pages"/>
				<xsl:call-template name="volume"/>
				<xsl:call-template name="series"/>
				<xsl:call-template name="place"/>
				<xsl:call-template name="idno"><xsl:with-param name="pType">DOI</xsl:with-param></xsl:call-template>
				<xsl:call-template name="idno"><xsl:with-param name="pType">ISBN</xsl:with-param></xsl:call-template>
				<xsl:call-template name="url"/>
				<xsl:call-template name="extra"/>
			</of:conferencePaperM>
		</of:conferencePaper>
	</xsl:template>

	<xsl:template match="biblStruct[@type='document' or @type='presentation' or @type='webpage' or @type='blogPost' or @type='videoRecording' or @type='film']" mode="inner">
		<of:document>
			<of:documentM>
				<xsl:call-template name="titleAndAuthors"/>
				<sp:date>
					<xsl:call-template name="GenerateYear"/>
				</sp:date>
				<xsl:call-template name="publisher"/>
				<xsl:if test="not(@type='document')">
					<sp:type>
						<xsl:choose>
							<xsl:when test="@type='presentation'">Présentation</xsl:when>
							<xsl:when test="@type='webpage'">Page web</xsl:when>
							<xsl:when test="@type='blogPost'">Post de blog</xsl:when>
							<xsl:when test="@type='videoRecording'">Vidéo</xsl:when>
							<xsl:when test="@type='film'">Film</xsl:when>
						</xsl:choose>
					</sp:type>
				</xsl:if>
				<xsl:call-template name="url"/>
				<xsl:call-template name="extra"/>
			</of:documentM>
		</of:document>
	</xsl:template>

	<xsl:template match="biblStruct[@type='report']" mode="inner">
		<of:report>
			<of:reportM>
				<xsl:call-template name="titleAndAuthors"/>
				<sp:date>
					<xsl:call-template name="GenerateYear"/>
				</sp:date>
			</of:reportM>
		</of:report>
	</xsl:template>

	<xsl:template match="biblStruct[@type='thesis']" mode="inner">
		<of:thesis>
			<of:thesisM>
				<xsl:call-template name="titleAndAuthors"/>
				<!-- university -->
				<sp:date>
					<xsl:call-template name="GenerateYear"/>
				</sp:date>
				<xsl:call-template name="thesisType"/>
				<xsl:call-template name="place"/>
				<xsl:call-template name="url"/>
				<xsl:call-template name="extra"/>
			</of:thesisM>
		</of:thesis>
	</xsl:template>
</xsl:stylesheet>