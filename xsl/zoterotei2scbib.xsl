<?xml version="1.0" encoding="UTF-8"?>
<!--
<a href="http://stph.crzt.fr">Stéphane Crozat</a> 
<a href="http://www.gnu.org/licenses/gpl-3.0.txt">License GPL3.0</a>
Jun 14, 2015
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"    
    version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    
    <xsl:output 
        indent="yes" 
        encoding="UTF-8"
        method="xml"/>
    
    
<!--****************************************************************
Return a standard bibliographic référence with the authors' names and 
the year of the publication. If there are two authors, they are both 
included, if there are more "et al." is added to the first name. 
Ex : 
Crozat, 2015
Crozat and Bachimont, 2015
Crozat et al., 2015
*****************************************************************-->
    <xsl:template name="GenerateRefName">  
        <!-- Retreives the list of the authors, without editors and contributors if possible -->
        <xsl:variable name="vAuthors">
            <xsl:call-template name="GenerateAuthorsList">
                <xsl:with-param name="pMode">authors-only</xsl:with-param>
                <xsl:with-param name="pNameMode">surname-only</xsl:with-param>
            </xsl:call-template>                
        </xsl:variable>   
        <!-- Test how many authors there is -->
        <xsl:variable name="vFirstAuthor" select="tokenize($vAuthors,', ')[1]"/>
        <xsl:variable name="vSecondAuthor" select="tokenize($vAuthors,', ')[2]"/>
        <xsl:variable name="vThirdAuthor" select="tokenize($vAuthors,', ')[3]"/>
        <xsl:choose>
            <!-- If no author identified, "Anonym." is returned-->
            <xsl:when test="not($vFirstAuthor)">
                Anonym.
            </xsl:when>            
            <!-- If one author, is name is returned-->
            <xsl:when test="not($vSecondAuthor)">
                <xsl:value-of select="$vFirstAuthor"/>
            </xsl:when>
            <!-- If two authors, their names are returned separated with coma -->
            <xsl:when test="not($vThirdAuthor)">
                <xsl:value-of select="$vFirstAuthor"/>
                <xsl:text> and </xsl:text>
                <xsl:value-of select="$vSecondAuthor"/>                
            </xsl:when>
            <!-- If more than two authors, first name is returned with "et al."-->
            <xsl:when test="$vThirdAuthor">
                <xsl:value-of select="$vFirstAuthor"/>
                <xsl:text> et al.</xsl:text>
            </xsl:when>
        </xsl:choose>
        <!-- Add coma -->
        <xsl:text>, </xsl:text>
        <!-- Add publication year-->
        <xsl:call-template name="GenerateYear"/>
    </xsl:template>
    
    
<!--****************************************************************
Return a standard bibliographic id with the first author name, the year
of the publication and the words of the title separated with underscores.
Accents, upper case and ponctuations are removed end URI encoding is performed.
Total length is limited to 100 characters.
Ex : 
Crozat-2012-Elements_pour_une_theorie_operationnelle
*****************************************************************-->
    <xsl:template name="GenerateRefId">
        <xsl:variable name="vId">
           <xsl:call-template name="GenerateRefIdNotEncoded"/>
        </xsl:variable>
        <xsl:variable name="vIdAsUri">
            <xsl:call-template name="ValueOfAsURI">
                <xsl:with-param name="pInput" select="$vId"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="substring($vIdAsUri,1,100)"></xsl:value-of>
    </xsl:template>
    
    <!-- Use externalized XSL ValueOfAsUri.xsl -->
    <xsl:include href="ValueOfAsUri.xsl"/>
    
    <xsl:template name="GenerateRefIdNotEncoded">     
        <!-- Number of words of the title to keep (desactivate with high value)-->
        <xsl:variable name="vWords" select="100"/>
        <!-- Retreives the list of the authors, without editors and contributors if possible -->
        <xsl:variable name="vAuthors">
            <xsl:call-template name="GenerateAuthorsList">
                <xsl:with-param name="pMode" select="authors-only"/>
                <xsl:with-param name="pNameMode">surname-only</xsl:with-param>                
            </xsl:call-template>                
        </xsl:variable>   
        <!-- Keep only first author name -->
        <xsl:value-of select="tokenize($vAuthors,', ')[1]"/>
        <!-- Add dash, year, dash -->
        <xsl:text>-</xsl:text>        
        <xsl:call-template name="GenerateYear"/>                        
        <xsl:text>-</xsl:text>     
        <!-- Add first words of the title -->           
        <xsl:for-each select="                        
            tokenize(
            normalize-space(.//title[parent::analytic or not(../preceding-sibling::*/title)][1]),
            '(\s)|(,)|(\.)|(:)|(;)|(\?)|(!)|('')|’|(&quot;)|(&amp;)|(\()|(\))|(\[)|(\])|(«)|(»)|(%)|(-)')
            [position()&lt;$vWords]">  
            <xsl:value-of select="."/>
            <xsl:if test="not(position()=$vWords or position()=last())">
                <xsl:text> </xsl:text>
            </xsl:if>
        </xsl:for-each>   
    </xsl:template>
    
    
<!--****************************************************************
Return the year of the publication 
Assuming YYYY, YYYY-MM or YYYY-MM-DD input format
If no date found, return n.d. (not dated)
*****************************************************************-->
    <xsl:template name="GenerateYear">
      <xsl:choose>            
          <!-- Prefering the date informed in monogr/imprint -->
          <xsl:when test="./monogr/imprint/date">
              <xsl:value-of select="substring(./monogr/imprint/date,1,4)"/>
          </xsl:when>
          <!-- Else any other date -->
          <xsl:when test=".//date">
              <xsl:value-of select="substring(.//date[1],1,4)"/>
          </xsl:when>              
          <!-- Else the year extracted from Web pade access date -->
          <xsl:when test=".//note[@type='accessed']">
              <xsl:value-of select="substring(.//note[@type='accessed'],1,4)"/>
          </xsl:when>
          <!-- Else return n.d. (not dated) -->
          <xsl:otherwise>
              <xsl:text>n.d.</xsl:text>
          </xsl:otherwise>
      </xsl:choose>                
    </xsl:template>
    
    <xsl:template name="GetName">  
    	<!-- if pMode set to surname-only, only surnames are returned -->      
    	<xsl:param name="pMode">surname-only</xsl:param>
        <xsl:choose>
            <!-- Return "Surname Forename" in that order separated with one space. -->
            <xsl:when test="surname">
                <xsl:value-of select="normalize-space(surname)"/>
                <xsl:if test="forename and $pMode!='surname-only'">
	                <xsl:text> </xsl:text>
	                <xsl:value-of select="normalize-space(forename)"/>
                </xsl:if>                                      
            </xsl:when>            
            <!-- if no surname, try to find a name attribute -->
            <xsl:otherwise>
                <xsl:value-of select="normalize-space(name)"/> 
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
<!--****************************************************************
Return the list of Authors separated with coma.
Add the editors to the list, except if param pMode is set to "authors-only"
Do not add the contributors to the list, except if param pMode is set to "all"
*****************************************************************-->
    <xsl:template name="GenerateAuthorsList">        
        <!-- authors-only, with-editors, all mode -->
        <xsl:param name="pMode">authors-only</xsl:param>
        <!--  cf GetName -->
        <xsl:param name="pNameMode">surname-forename</xsl:param>
        <!-- Authors -->
        <xsl:for-each select=".//author">
            <xsl:call-template name="GetName">
            	<xsl:with-param name="pMode" select="$pNameMode"/>
            </xsl:call-template>
            <xsl:if test="not(position()=last())">
                <xsl:text>, </xsl:text>
            </xsl:if>           
        </xsl:for-each>
        <!-- Editor are added if pMode set or no author found --> 
        <xsl:if test="$pMode='with-editors' or not(.//author)">
            <xsl:if test=".//editor and .//author">
                <xsl:text>, </xsl:text>
            </xsl:if>
            <xsl:for-each select=".//editor">
                <xsl:call-template name="GetName">
	            	<xsl:with-param name="pMode" select="$pNameMode"/>
	            </xsl:call-template>      
	            <xsl:if test="$pNameMode!='surname-only'">          
                	<xsl:text> (ed)</xsl:text>
                </xsl:if>
                <xsl:if test="not(position()=last())">
                    <xsl:text>, </xsl:text>
                </xsl:if>   
            </xsl:for-each>
        </xsl:if>
        <!-- Contributors are added if pMode set or no author nor editor found -->
        <xsl:if test="$pMode='all' or not(.//author | .//editor)">
            <xsl:if test=".//persName and (.//author | .//editor)">
                <xsl:text>, </xsl:text>
            </xsl:if>
           <xsl:for-each select=".//persName">               
               <xsl:call-template name="GetName">
            	   <xsl:with-param name="pMode" select="$pNameMode"/>
               </xsl:call-template>     
               <xsl:if test="$pNameMode!='surname-only'">
	               <xsl:text> (</xsl:text>
	               <xsl:choose>
		               <xsl:when test="..//resp='presenter'">
		               		<xsl:text>pres</xsl:text>
		               </xsl:when>
		               <xsl:otherwise>
		               		<xsl:text>contrib</xsl:text>
		               </xsl:otherwise>
	               </xsl:choose>	               	               
	               <xsl:text>)</xsl:text>               	   
               </xsl:if>	
               <xsl:if test="not(position()=last())">
                   <xsl:text>, </xsl:text>
               </xsl:if>   
           </xsl:for-each>
        </xsl:if>
    </xsl:template>
    
    
<!--****************************************************************
Main
*****************************************************************-->
    <xsl:template match="/">
        <xsl:processing-instruction name="xml-stylesheet">type"text/xsl" href="scbib2optim.xsl"</xsl:processing-instruction>        
        <bib>
            <xsl:apply-templates select=".//biblStruct"></xsl:apply-templates>
        </bib>
    </xsl:template>
    
    <xsl:template match="biblStruct">
        <ref type="{@type}">
            <ref-id>
                <xsl:call-template name="GenerateRefId"/>
            </ref-id>            
            <ref-name>
                <xsl:call-template name="GenerateRefName"/>
            </ref-name>                                       
            <authors>
                <xsl:call-template name="GenerateAuthorsList"/>                    
            </authors>                            
            <year>
                 <xsl:call-template name="GenerateYear"/>     
            </year>
            <title>
                <xsl:apply-templates select=".//title[parent::analytic or not(../preceding-sibling::*/title)]" mode="MainTitle"/>
            </title>
            <in-title>
                <xsl:apply-templates select=".//title[parent::monogr and ../preceding-sibling::*/title]" mode="InTitle"/>
            </in-title>
            <edition>
                <xsl:apply-templates select=".//edition | .//publisher | .//pubPlace | .//note[@type='thesisType']"/>
            </edition>
            <scope>
                <xsl:apply-templates select=".//biblScope"/>
            </scope>
            <url>
                <xsl:apply-templates select=".//note[@type='url']"/>           
            </url>
            <structured-data>
                <xsl:apply-templates select=".//author | .//editor | .//persName"/>          
            </structured-data>
        </ref>
    </xsl:template>    
    
    <xsl:template match="author | editor | persName">
        <xsl:copy-of select="."/>            
    </xsl:template>
        
    <xsl:template match="title" mode="MainTitle">        
        <xsl:value-of select="normalize-space(.)"/>        
    </xsl:template>
    
    <xsl:template match="title" mode="InTitle">        
        <xsl:if test="text()!=''">
            <xsl:text>in </xsl:text> 
        </xsl:if>
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>
    
    <xsl:template match="edition | publisher | pubPlace | note[@type='thesisType']">
        <xsl:value-of select="normalize-space(.)"/> 
        <xsl:if test="following-sibling::edition | following-sibling::publisher | following-sibling::pubPlace | following-sibling::note[@type='thesisType'] 
            | following-sibling::imprint[edition | publisher | pubPlace | note[@type='thesisType']]">
            <xsl:text>, </xsl:text>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="biblScope">
            <xsl:choose>
                <xsl:when test="@unit='page'">
                    <xsl:text>pp</xsl:text>
                </xsl:when>
                <xsl:when test="@unit='issue'">
                    <xsl:text>n°</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="substring(normalize-space(@unit),1,3)"/>
                    <xsl:text>.</xsl:text>
                </xsl:otherwise>
            </xsl:choose>                        
            <xsl:value-of select="normalize-space(.)"/>
            <xsl:if test="following-sibling::biblScope">
                <xsl:text> </xsl:text>
            </xsl:if>        
    </xsl:template>    
    
    <xsl:template match="note[@type='url']">
          <xsl:if test="preceding-sibling::note[@type='accessed']">
              <xsl:attribute name="accessed" select="preceding-sibling::note[@type='accessed'][1]"/>            
          </xsl:if>    
          <xsl:value-of select="normalize-space(.)"/>        
    </xsl:template>
        
    <xsl:template match="*">
        <xsl:comment><xsl:value-of select="normalize-space(.)"></xsl:value-of></xsl:comment>        
    </xsl:template>
    
    
    
</xsl:stylesheet>